# Déploiement d'une application Rails sans interruption de service

## Visualiser cette présentation en ligne

- [SpeackerDeck](https://speakerdeck.com/bobmaerten/deploiement-dune-application-rails-sans-interruption-de-service)

## Visualiser cette présentation en local

### Clonez le dépôt

    git clone https://gitlab.com/bobmaerten/presentation-zdd.git

### Lancez votre navigateur préféré

- Linux : `sensible-browser presentation-zdd/index.html`
- MacOS : `open presentation-zdd/index.html`
